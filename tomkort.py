# -*- coding: utf-8 -*-

"""
    Copyright (c) 2015 by Tomi Kortelainen


    ## tomkort.net ##

    Homepage for Tomi Kortelainen.

"""

__author__ = 'Tomi Kortelainen'


import os
from flask import Flask, request, url_for, session, g, redirect, \
                  abort, flash, render_template


app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html', subtitle='Index')

@app.route('/projects/')
def projects():
    return render_template('projects.html', subtitle='Projects')

@app.route('/about')
def about():
    return render_template('about.html', subtitle='About me')

@app.route('/contact')
def contact():
    return render_template('contact.html', subtitle='Contact me')

# Start debug server if run directly.
if __name__ == '__main__':
    app.run(debug=True)


